#Visión por computadoras-TP3: Rectificando imágenes
#Estudiante: Tolaba Bruno Ignacio
#Legajo: 80869
#UTN-FRC

import cv2                                                             
import numpy as np

img1=cv2.imread("Imagen1.jpg")                                      #Se lee la imagen

puntos = []

def cuatro_puntos(event, x, y, flags, param):                       #Función para seleccionar 4 puntos en la imagen.
    global puntos
    if event == cv2.EVENT_LBUTTONDOWN and len(puntos) < 4:          #Si se presiona click izq y si es menor a 4 la lista.
        cv2.circle(img1, (x, y), 5, (0, 255, 0), 0)
        puntos.append((x, y))                                       #Agrego los puntos a la lista.
        cv2.imshow("Imagen Original", img1)

def homografia (img1, puntos_img1):                                 #Función donde se calcula la matríz de homografía.
    global matriz,m,n
    m,n = img1.shape[:2]
    puntos_img2 = np.float32([[0, 0], [n, 0], [n, m], [0, m]])      #Tamaño de la segunda img en función del tamaño de img1.
    matriz = cv2.getPerspectiveTransform(puntos_img1, puntos_img2)  #Se obtiene la matríz.

cv2.imshow("Imagen Original", img1)
cv2.setMouseCallback("Imagen Original", cuatro_puntos)

while len(puntos) < 4:              
    cv2.waitKey(1)

cv2.destroyAllWindows()

puntos_img1 = np.float32(puntos)
homografia(img1,puntos_img1)                                        #Llamado a la función


img_resultado = cv2.warpPerspective(img1, matriz, (n, m))           #Aplicamos la transformación.
cv2.imwrite('ImagenFinal.png',img_resultado)                        #Se guarda la imagen final.
cv2.imshow("Imagen Final", img_resultado)                           #Se muestra la imagen final.
cv2.waitKey(0)
cv2.destroyAllWindows()
