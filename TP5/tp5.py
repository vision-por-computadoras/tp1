#Visión por computadoras-TP5: El programa mide el largo y ancho de objetos distintos, utilizando un marcador aruco. Para ello diferencia los objetos de un fondo blanco.
#Estudiante: Tolaba Bruno Ignacio
#Legajo: 80869
#UTN-FRC

import cv2
import numpy as np


def deteccion_objetos(frame):

    gray=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)                                                     #Pasamos la img a escala de grises.                                           

    mask=cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV,19,5)     #Mascara binaria (blanco o negro) para diferenciar el fondo de los obj. 
    contornos,_=cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)                    #Encuentra los contornos del obj.

    objetos_contornos=[]

    for cnt in contornos:                                                                           #Recorremos los contornos encontrados.
        area=cv2.contourArea(cnt)                                                                   #Obtenemos el área
        if area>2000:                                                   
            objetos_contornos.append(cnt)                                                           #Si el área es relativamente grande lo guardamos como si fuese un obj.

    return objetos_contornos


parametros = cv2.aruco.DetectorParameters_create()                                  #Objeto que detecta arucos.
diccionario = cv2.aruco.Dictionary_get(cv2.aruco.DICT_5X5_100)                      #Objeto diccionario de 5x5.

cap=cv2.VideoCapture(0)                                                             #Capturamos la camara de la pc.

while True:

    ret, frame = cap.read()                                                         #Obtenemos los fotogramas de la camara.
    
    if not ret:                                                                     #Si no puede acceder a la camara, cerramos el programa.
        print("No se pudo abrir la camara.")                                     
        break

    esquinas,_,_= cv2.aruco.detectMarkers(frame,diccionario,parameters=parametros)  #Obtenemos las coordenadas de las esquinas del marcador aruco.
    esquinas_ent=np.int0(esquinas)                                                  #Pasamos a valores enteros la lista esquinas.
    cv2.polylines(frame,esquinas_ent,True,(0,0,255),5)                              #Se traza el contorno del marcador aruco.

    perimetro_aruco=cv2.arcLength(esquinas_ent[0],True)                             #Obtenemos el perímetro en pixeles del aruco.

    proporcion_cm=perimetro_aruco/40                                                #Pasamos a cm, dividiendolo por el perímetro en cm del aruco.             

    contornos=deteccion_objetos(frame)                                              #Extraemos los contornos de los objetos.
    
    for cont in contornos:                                                          #Recorremos los contornos
        rectangulo=cv2.minAreaRect(cont)                                            #Extraemos un rectangulo del objeto (para luego obtener la altura y ancho del mismo)
        (x,y),(an,al),angulo=rectangulo                                             #Obtenemos el ancho y alto.

        ancho=an/proporcion_cm                                                      #Pasamos el ancho en pixeles a cm.
        alto=al/proporcion_cm                                                       #Pasamos el alto en pixeles a cm.

        cv2.circle(frame,(int(x),int(y)),5,(255,255,0),-1)                          #Se muestra un circulo en el centro del rectangulo.
        
        rect=cv2.boxPoints(rectangulo)                                              #Obtenemos el rectangulo.
        rect=np.int0(rect)                                                          #Se pasa el rectangulo a entero.

        cv2.polylines(frame,[rect],True,(0,255,0),2)                                #Se dibuja el rectangulo en la captura. 

        cv2.putText(frame,"Ancho: {} cm".format(round(ancho,1)),(int (x),int(y-15)),cv2.FONT_HERSHEY_SIMPLEX, 0.8, (150,0,255),2)   #Mostramos la información de ancho y altura.
        cv2.putText(frame,"Largo: {} cm".format(round(alto,1)),(int (x),int(y+15)),cv2.FONT_HERSHEY_SIMPLEX, 0.8, (150,0,255),2)

    cv2.imshow("Medicion de Objetos",frame)                                         #Se muestra la captura.

    t=cv2.waitKey(1)
    if t == 27:                                                                     #Salir con 'esc'
        break

cap.release()                                                                       
cv2.destroyAllWindows()

