#Visión por computadoras-TP1: Eventos y callbacks
#Tolaba Bruno Ignacio
#Legajo: 80869
#UTN-FRC


import cv2                                                              #Importamos módulo de OpenCV
import numpy as np                                                      #Importamos módulo de Numpy

drawing = False                                                         #Bandera para dibujar el rectangulo
ix, iy = -1, -1                                                         #Almacena primer click para dibujar el rectangulo
fx,fy=-1,-1                                                             #Almacena donde termina el rectangulo. 

img = cv2.imread('images.png')                                          #Leemos la imagen.

def draw_rect(event, x, y, flags, param):                               #Función para dibujar rectangulo.
    global fx,fy, ix, iy, drawing, mode                                 #Variables globales.

    if event == cv2.EVENT_LBUTTONDOWN:                                  #Si se presiona el click izquierdo:
        drawing = True                                                  #Inicio del rectangulo
        ix, iy = x, y                                                   #Almacenamos el punto de inicio del rectangulo.

    elif event == cv2.EVENT_MOUSEMOVE:                                  #Si se mueve el mouse:
        if drawing is True:                 
            img_aux=img.copy()                                          #Copio la imagen original
            cv2.rectangle(img_aux, (ix, iy), (x, y), (0, 255, 0), 1)    #Grafico el rectangulo
            cv2.imshow('image',img_aux)                                 #Mostramos la imagen auxiliar

    elif event == cv2.EVENT_LBUTTONUP:                                  #Dejo de presionar el botón izquierdo:
        drawing = False                                                  
        fx, fy=x,y                                                      #Guardo el último punto
        cv2.rectangle(img, (ix, iy), (fx, fy), (0, 0, 0 ,0),0)
         

cv2.namedWindow('image')                                                
cv2.setMouseCallback('image', draw_rect)                                #Llamamos a la función en el callback

while(1):
    
    cv2.imshow('image', img)
    k = cv2.waitKey(1) & 0xFF
    if k == ord('g'):                                                   #Se presiona g para recortar y guardar la img:
        imgf=img.copy()
        imgf=img[iy+1:fy,ix+1:fx]                                           #Recortamos la imagen según los ptos final e inicial
        cv2.imwrite('imagenfinal.png',imgf)                             #Guardamos como tal la imagen final.
        break
    elif k == ord('r'):                                                 #Restauramos la img para rehacer el rectangulo
        img=cv2.imread('images.png')
    elif k == ord('q'):                                                 #Salir al presionar q:
        break
cv2.destroyAllWindows()
