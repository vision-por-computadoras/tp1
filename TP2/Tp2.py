#Visión por computadoras-TP2: Transformación Afín
#Estudiante: Tolaba Bruno Ignacio
#Legajo: 80869
#UTN-FRC

import cv2                                                             
import numpy as np

img1=cv2.imread("Imagen1.jpg")
img2=cv2.imread("Imagen2.jpg")

puntos = []

def tres_puntos(event, x, y, flags, param):                     #Función para seleccionar 3 puntos en la imagen.
    global puntos
    if event == cv2.EVENT_LBUTTONDOWN and len(puntos) < 3:      #Si se presiona click izq y si es menor a 3 la lista.
        cv2.circle(img1, (x, y), 5, (0, 255, 0), 0)
        puntos.append((x, y))                                   #Agrego los puntos a la lista.
        cv2.imshow("Imagen Original", img1)

def transf_afin (img2):                                         #Función donde se realiza la transf afín.
    global img2_transformada
    esquinas_img2 = np.float32([[0,0],[img2.shape[1],0],[img2.shape[1],img2.shape[0]]]) #esq sup-izq, sup-der, inf-der.
    puntos_img1 = np.float32(puntos)                            #Transformo la lista a punto flotante de 32 bits.
    M=cv2.getAffineTransform(esquinas_img2,puntos_img1)         #Se crea la matriz de transformación afín.
    h, w = img1.shape [:2]                                      #Se obtiene el alto y ancho de la img.
    img2_transformada=cv2.warpAffine(img2,M,(w,h))              #Se aplica la transformación afín.

def img_final(img1):                                            #Función para obtener la imagen final.
    global img_resultado    
    mascara= np.where(img2_transformada != 0, 255, 0).astype(np.uint8)  #Se crea una mascara.  
    img1=cv2.bitwise_and(img1,cv2.bitwise_not(mascara))                 #Se aplica la mascara a la img1. 
    img_resultado = cv2.add (img1, img2_transformada)                   #Se suman las dos imagenes.

cv2.imshow("Imagen Original", img1)
cv2.setMouseCallback("Imagen Original", tres_puntos)

while len(puntos) < 3:              
    cv2.waitKey(1)

cv2.destroyAllWindows()

transf_afin (img2)
img_final(img1)

cv2.imwrite('ImagenFinal.png',img_resultado)
cv2.imshow("Imagen Final", img_resultado)                       #Se muestra la imagen final.
cv2.waitKey(0)
cv2.destroyAllWindows()
