#Visión por computadoras-TP4:
#Estudiante: Tolaba Bruno Ignacio
#Legajo: 80869
#UTN-FRC

import cv2 
import numpy as np
import math

drawing = False
ix,iy=0,0


def homog (image, origen, destino):                             #Función de transformación

    (altura, ancho) = image.shape [:2]

    M = cv2.getPerspectiveTransform (origen, destino)           #Matriz de perspectiva
    
    img_out = cv2.warpPerspective (image, M, (ancho,altura))    #Transformación perspectiva

    return img_out


def medicion (event, x, y, flags, param):                       #Función para mediciones
    global ix,iy,drawing,img_rect,img_rec_copia,img_medida

    if event==cv2.EVENT_LBUTTONDOWN:                            #Inicio de medición
        ix,iy=x,y
        drawing = True

    elif event == cv2.EVENT_MOUSEMOVE:                          #Grafica mientras  se mueve el mouse
        if drawing is True:
            img_rect[:]=img_medida[:]
            cv2.line(img_rect,(ix,iy),(x,y),(0,0,255),1)
            medida=(np.sqrt((ix-x)**2+(iy-y)**2))               #Distancia entre dos puntos
            medida=medida/12                                    #Divido por 12 porque se agrandó la imagen x12
            x_y_text=int((ix+x)/2+10),int ((iy+y)/2-10)         #Coordenadas del texto
            cv2.putText(img_rect,"{:.2f} cm".format(medida),(x_y_text),cv2.FONT_HERSHEY_PLAIN,1,(0,0,255),1)
    
    elif event ==cv2.EVENT_LBUTTONUP:                           #Finaliza medición
        img_medida[:]=img_rect[:]
        drawing = False


image = cv2.imread ('image.jpeg')                               #Importa imagen

(h,w)=(276,468)                                                 #23cm x 12; 39cm x 12 Tamaño del marco de la pantalla x 12 para agrandar.

(x1,y1)=(573,235)                                               #Vertice sup-izq del borde de la pantalla
(x2,y2)=(1153,189)                                              #Vertice sup-der del borde de la pantalla
(x3,y3)=(564,548)                                               #Vertice inf-izq del borde de la pantalla
(x4,y4)=(1135,598)                                              #Vertice inf-der del borde de la pantalla


destino = np.float32(([[x1,y1],[x1+w,y1],[x1,y1+h],[x1+w,y1+h]]))   #Vertices con offset
origen = np.float32([[x1,y1],[x2,y2],[x3,y3],[x4,y4]])              #Vertices originales

img_rect = homog (image, origen, destino)                           #Llama a la función
img_rect_copia = img_rect.copy()
img_medida = img_rect.copy()

cv2.imwrite("ImagenTransformada.jpeg",img_rect)                   #Se guarda la imagen transformada

cv2.namedWindow("Calibrada para medición")
cv2.setMouseCallback("Calibrada para medición", medicion)       

while True:

    cv2.imshow("Calibrada para medición", img_rect)             #Visualización de imagen transformada

    k=cv2.waitKey(1) & 0xFF

    if k == ord ('q'):                                          #Salir
        break   
    if k == ord ('r'):                                          #Se restaura la imagen
        img_rect = img_rect_copia.copy()
        img_medida = img_rect.copy()


cv2.imwrite("ImagenMediciones.jpeg",img_rect)                   #Se guarda la imagen con mediciones
cv2.destroyAllWindows()



